package com.evelopers;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import org.apache.commons.cli.*;
import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by edward on 04.06.2020.
 */
public class SqsClient {

    static String test_accessKey="AKIAQQG4GK2N2KQC2LEC";
    static String test_secretKey="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";
    static String test_regionName="eu-west-1";

    private static final String qaQueueUrl = "https://sqs.eu-west-1.amazonaws.com/034821527195/alfresco-2-datahub-qa.fifo";
    private static final String prodQueueUrl = "https://sqs.eu-west-1.amazonaws.com/034821527195/alfresco-2-datahub-prod.fifo";

    // To prevent unwanted throttles on the function that can cause messages to fall to DLQ
    // before they are processed, set the maximum concurrency on the event source.
    // However, the minimum concurrency allowed is 2, which may result in write conflicts on datahub
    // as it only allows a single concurrent write to a given repository.
    // I contacted AWS support to see if there is a way to constrain the concurrency
    // without using reserved concurrency, and they said that using a FIFO queue
    // where all messages in the queue are assigned the same MessageGroupID
    // would effectively serialize the messages batches
    private static final String MESSAGE_GROUP_ID = "datahub";

    public static void main(String[] args) {


        Options options = new Options();

        Option action = new Option("a", "action", true, "action to perform [\"UPDATE\",\"DELETE\"]");
        action.setRequired(true);
        options.addOption(action);

        Option publicationId = new Option("pid", "publicationId", true, "publicationId value");
        publicationId.setRequired(true);
        options.addOption(publicationId);

        Option env = new Option("env", "environment", true, "environment value [\"qa\",\"prod\"]");
        env.setRequired(true);
        options.addOption(env);

        Option accessKey = new Option("ak", "accessKey", true, "accessKey value");
        accessKey.setRequired(true);
        options.addOption(accessKey);

        Option secretKey = new Option("sk", "secretKey", true, "secretKey value");
        secretKey.setRequired(true);
        options.addOption(secretKey);

        Option regionName = new Option("rn", "regionName", true, "regionName value");
        regionName.setRequired(true);
        options.addOption(regionName);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
        }

        String actionType = cmd.getOptionValue("action");
        String publicationIdValue = cmd.getOptionValue("publicationId");
        String envValue = cmd.getOptionValue("environment");
        String accessKeyValue = cmd.getOptionValue("accessKey");
        String secretKeyValue = cmd.getOptionValue("secretKey");
        String regionNameValue = cmd.getOptionValue("regionName");

        System.out.println(actionType);
        System.out.println(publicationIdValue);


        AWSCredentials credentials = new BasicAWSCredentials(accessKeyValue, secretKeyValue);
        final AmazonSQS sqsClient = AmazonSQSClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(regionNameValue)
                .build();

        Map<String, MessageAttributeValue> attributes = new HashMap<String, MessageAttributeValue>(2);

        MessageAttributeValue actionAttributeValue = new MessageAttributeValue();
        actionAttributeValue.setStringValue(actionType);
        actionAttributeValue.setDataType("String");
        attributes.put("action", actionAttributeValue);

        MessageAttributeValue publicationIdAttributeValue = new MessageAttributeValue();
        publicationIdAttributeValue.setStringValue(publicationIdValue);
        publicationIdAttributeValue.setDataType("String");
        attributes.put("documentId", publicationIdAttributeValue);

        String messageBody = publicationIdValue + "/" + actionType + "/" + (new DateTime()).getMillis();

        String queueUrl = "";
        if ("prod".equals(envValue)) {
            queueUrl = prodQueueUrl;
        } else if ("qa".equals(envValue)) {
            queueUrl = qaQueueUrl;
        }

        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageGroupId(MESSAGE_GROUP_ID)
                .withMessageBody(messageBody)
                .withMessageAttributes(attributes);
        sqsClient.sendMessage(send_msg_request);

    }

}
